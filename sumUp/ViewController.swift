//
//  ViewController.swift
//  sumUp
//
//  Created by HigherVisibility on 28/08/2018.
//  Copyright © 2018 ahmedHigherVisibility. All rights reserved.
//

import UIKit
import SumUpSDK
import OAuthSwift
import SwiftKeychainWrapper
import SafariServices


class ViewController:  OAuthViewController{
    var oauthswift: OAuth2Swift?
//    var urlToHandle:URL?
//
//    lazy var internalWebViewController: WebViewController = {
//        let controller = WebViewController()
//
//
//        controller.view = UIView(frame: UIScreen.main.bounds) // needed if no nib or not loaded from storyboard
//
//        controller.delegate = self
//        controller.viewDidLoad() // allow WebViewController to use this ViewController as parent to be presented
//        return controller
//    }()

    //MARK: OutletS
    
    @IBOutlet weak var logoutBtn_outlet: UIButton!
    @IBOutlet weak var loginBtn_outlet: UIButton!
    
    @IBOutlet weak var textFieldTotal: UITextField!
    
    @IBOutlet weak var textFieldTitle: UITextField!
    
    @IBOutlet weak var label: UILabel!
    
    
    //MARK:View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.textFieldTotal.delegate = self
        self.textFieldTitle.delegate = self
        
        self.textFieldTotal.layer.borderColor = UIColor.black.cgColor
        self.textFieldTotal.layer.borderWidth = 2
        
        self.textFieldTitle.layer.borderColor = UIColor.black.cgColor
        self.textFieldTitle.layer.borderWidth = 2
        
       self.updateButtonStates()
        
    }

    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
      
        print(SumUpSDK.isLoggedIn)
        if let token = KeychainWrapper.standard.string(forKey: "accesstoken"){
            print("NOTE: ID found in keychain \(token)")
           loginwithsumup(token: token)
            
        }
       
        
        
//        logoutBtn_outlet.isEnabled = SumUpSDK.isLoggedIn
//        logoutBtn_outlet.isHighlighted = false
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//
//        print(SumUpSDK.isLoggedIn)
//        if let token = KeychainWrapper.standard.string(forKey: "accesstoken"){
//            print("NOTE: ID found in keychain \(token)")
//            SumUpSDK.login(withToken: token, completion: { (Bool, Error) in
//                print("successful loggin with acesstoken")
//                if let error = Error{
//                    print(error.localizedDescription)
//                }
//                self.updateButtonStates()
//
//            })
//        }
//
//
//        //        logoutBtn_outlet.isEnabled = SumUpSDK.isLoggedIn
//        //        logoutBtn_outlet.isHighlighted = false
//    }
    
    
    
     //MARK:Button Action
    
    func updateButtonStates() {
        
        let isLoggedIn = SumUpSDK.isLoggedIn
        print(isLoggedIn)
        if isLoggedIn == false {
            self.loginBtn_outlet.isEnabled = !isLoggedIn
            self.loginBtn_outlet.isHidden = isLoggedIn
            
            logoutBtn_outlet.isEnabled = isLoggedIn
            logoutBtn_outlet.isHidden = !isLoggedIn
        }
        else{
            //        self.logoutBtn_outlet.isUserInteractionEnabled  = isLoggedIn
            self.loginBtn_outlet.isEnabled = !isLoggedIn
            self.loginBtn_outlet.isHidden = isLoggedIn
            
            logoutBtn_outlet.isEnabled = isLoggedIn
            logoutBtn_outlet.isHidden = !isLoggedIn
            
        }
    }
    
    
    func showTokenAlert( credential: OAuthSwiftCredential) {
        var message = "oauth_token:\(credential.oauthToken)"
        if !credential.oauthTokenSecret.isEmpty {
            message += "\n\noauth_token_secret:\(credential.oauthTokenSecret)"
        }
        
        
        
        
    }
    
    @IBAction func preferenceBtn_action(_ sender: Any) {
        
      self.presentCheckoutPreferences()
        
    }
    
    
    @IBAction func chargeBtn_action(_ sender: Any) {
        
        self.requestCheckout()
        
    }
    
//    func getURLHandler()-> OAuthSwiftURLHandlerType{
//        if internalWebViewController.parent == nil {
//            self.addChild(internalWebViewController)
//        }
//        return internalWebViewController
//
//    }
////
//    func handle(_ url:URL){
//        self.urlToHandle = url
//
//        let url = self.urlToHandle
//
//        let controller = WebViewController()
//        controller.targetURL = url
//
//
//
//    }
    
    
//    @IBAction func signinwithtoken(_ sender: Any) {
//        print("safasfasafasff")
//        if let token = KeychainWrapper.standard.string(forKey: "accesstoken"){
//            print("NOTE: ID found in keychain \(token)")
//            SumUpSDK.login(withToken: token, completion: { (Bool, Error) in
//
//                if let error = Error{
//                    print(" the error are as follows \(error.localizedDescription)")
//                }
//                self.updateButtonStates()
//
//            })
//        }
//
//    }
    
    @IBAction func loginBtn_action(_ sender: Any) {
        
        self.oauthswift = OAuth2Swift(
            consumerKey: "0LM_BHtWahzTC7RrOddJ4D8l4S12",
            consumerSecret: "ef5be9e7d817e730f5422724860f6e869ebf302c84399e0fe2cbbbb794dc64f8",
            authorizeUrl:   "https://api.sumup.com/authorize",
            accessTokenUrl: "https://api.sumup.com/token",
            responseType:   "code"
        )
        
        self.oauthswift!.allowMissingStateCheck = true
        //2
   self.oauthswift!.authorizeURLHandler = SafariURLHandler(viewController: self, oauthSwift: oauthswift!)
        
        guard let rwURL = URL(string: "sumup://oauth-callback/sumup") else { return }
        
        //3
        self.oauthswift!.authorize(withCallbackURL: rwURL, scope: "", state: "", success: {
            (credential, response, parameters) in
            let keychainresult = KeychainWrapper.standard.set(credential.oauthToken, forKey: "accesstoken")
            print("data saved to keychain \(keychainresult)")
            
//            self.loginwithsumup(token : credential.oauthToken)
            
        }, failure: { (error) in
            print("alert\(error.localizedDescription)")
        })
    }
    
    func loginwithsumup(token:String){
        SumUpSDK.login(withToken: token, completion: { (Bool, Error) in
            if let error = Error{
                print(error.localizedDescription)
            }
                
            else {
                print("fahsfgjasgjagsfjasahsjafg")
                self.updateButtonStates()
                
            }
        })
        
        
    }
    
    @IBAction func signOutBtn_action(_ sender: Any) {
        
        
         self.requestLogout()
        
    }
    
//    private func presentLogin() {
//        // present login UI and wait for completion block to update button states
//        SumUpSDK.presentLogin(from: self, animated: true) { [weak self] (success: Bool, error: Error?) in
//            print("Did present login with success: \(success). Error: \(String(describing: error))")
//
//            guard error == nil else {
//                // errors are handled within the SDK, there should be no need
//                // for your app to display any error message
//                return
//            }
//            print(SumUpSDK.isLoggedIn)
//            let keychainresult = KeychainWrapper.standard.set(SumUpSDK.isLoggedIn, forKey: "accesstoken")
//            print("data saved to keychain \(keychainresult)")
//
//
//            // self?.updateCurrency()
//            self?.updateButtonStates()
//        }
//    }
    
    fileprivate func showResult(string: String) {
        label?.text = string
        // fade in label
        UIView.animateKeyframes(withDuration: 3, delay: 0, options: [.allowUserInteraction, .beginFromCurrentState], animations: {
            let relativeDuration = TimeInterval(0.15)
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: relativeDuration) {
                self.label?.alpha = 1.0
            }
            UIView.addKeyframe(withRelativeStartTime: 1.0 - relativeDuration, relativeDuration: relativeDuration) {
               self.label?.alpha = 0.0
            }
        }, completion: nil)
    }
    
    
    private func presentCheckoutPreferences() {
        SumUpSDK.presentCheckoutPreferences(from: self, animated: true) { [weak self] (success: Bool, presentationError: Error?) in
            print("Did present checkout preferences with success: \(success). Error: \(String(describing: presentationError))")
            
            guard let safeError = presentationError as NSError? else {
                // no error, nothing else to do
                return
            }
            
            print("error presenting checkout preferences: \(safeError)")
            
            let errorMessage: String
            switch (safeError.domain, safeError.code) {
            case (SumUpSDKErrorDomain, SumUpSDKError.accountNotLoggedIn.rawValue):
                errorMessage = "not logged in"
                
            case (SumUpSDKErrorDomain, SumUpSDKError.checkoutInProgress.rawValue):
                errorMessage = "checkout is in progress"
                
            default:
                errorMessage = "general error"
            }
            
            self?.showResult(string: errorMessage)
        }
    }
    
    fileprivate func requestLogout() {
        
        SumUpSDK.logout { [weak self] (success: Bool, error: Error?) in
            
            let storage = HTTPCookieStorage.shared
            if let cookies = storage.cookies {
                for cookie in cookies {
                    storage.deleteCookie(cookie)
                }
            }
            
            
            
            
//            print("Did log out with success: \(success). Error: \(String(describing: error))")
       self?.updateButtonStates()
//            let keychainresult = KeychainWrapper.standard.removeAllKeys()
//            print("id removed from keychain\(keychainresult)")
//            print("succesful logging out ")
            print(SumUpSDK.isLoggedIn)
        }
    }
    
    
    
    
    
    fileprivate func requestCheckout() {
        
        // ensure that we have a valid merchant
        guard let merchantCurrencyCode = SumUpSDK.currentMerchant?.currencyCode else {
            showResult(string: "not logged in")
            return
        }
        
        guard let totalText = textFieldTotal?.text else {
            return
        }
        
        // create an NSDecimalNumber from the totalText
        // please be aware to not use NSDecimalNumber initializers inherited from NSNumber
        let total = NSDecimalNumber(string: totalText)
        guard total != NSDecimalNumber.zero else {
            return
        }
        
        // setup payment request
        let request = CheckoutRequest(total: total,
                                      title: textFieldTitle?.text,
                                      currencyCode: merchantCurrencyCode,
                                      paymentOptions: [.cardReader, .mobilePayment])
        
//        // add tip if selected
//        if let selectedTip = segmentedControlTipping?.selectedSegmentIndex,
//            selectedTip > 0,
//            tipAmounts.indices ~= selectedTip {
//            let tipAmount = tipAmounts[selectedTip]
//            request.tipAmount = tipAmount
//        }
//
//        // set screenOptions to skip if switch is set to on
//        if let skip = switchSkipReceiptScreen?.isOn, skip {
//            request.skipScreenOptions = .success
//        }
        
        // the foreignTransactionID is an **optional** parameter and can be used
        // to retrieve a transaction from SumUp's API. See -[SMPCheckoutRequest foreignTransactionID]
        request.foreignTransactionID = "your-unique-identifier-\(ProcessInfo.processInfo.globallyUniqueString)"
        
        SumUpSDK.checkout(with: request, from: self) { [weak self] (result: CheckoutResult?, error: Error?) in
            if let safeError = error as NSError? {
                print("error during checkout: \(safeError)")
                
                if (safeError.domain == SumUpSDKErrorDomain) && (safeError.code == SumUpSDKError.accountNotLoggedIn.rawValue) {
                    self?.showResult(string: "not logged in")
                } else {
                    self?.showResult(string: "general error")
                }
                
                return
            }
            
            guard let safeResult = result else {
                print("no error and no result should not happen")
                return
            }
            
            print("result_transaction==\(String(describing: safeResult.transactionCode))")
            
            if safeResult.success {
                print("success")
                var message = "Thank you - \(String(describing: safeResult.transactionCode))"
                
                if let info = safeResult.additionalInfo,
                    let tipAmount = info["tip_amount"] as? Double, tipAmount > 0,
                    let currencyCode = info["currency"] as? String {
                    message = message.appending("\ntip: \(tipAmount) \(currencyCode)")
                }
                
                self?.showResult(string: message)
            } else {
                print("cancelled: no error, no success")
                self?.showResult(string: "No charge (cancelled)")
            }
        }
        
        // after the checkout is initiated we expect a checkout to be in progress
        if !SumUpSDK.checkoutInProgress {
            // something went wrong: checkout was not started
            showResult(string: "failed to start checkout")
        }
    }

}


    




extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldTotal {
            // we assume a checkout is imminent
            // let the SDK know to e.g. wake a connected terminal
            SumUpSDK.prepareForCheckout()
            
            textFieldTitle?.becomeFirstResponder()
        } else if SumUpSDK.isLoggedIn {
            requestCheckout()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
extension ViewController: OAuthWebViewControllerDelegate {
    
    
    func oauthWebViewControllerDidPresent() {
        
    }
    func oauthWebViewControllerDidDismiss() {
        
    }
    
    
    func oauthWebViewControllerWillAppear() {
        
    }
    func oauthWebViewControllerDidAppear() {
        
    }
    func oauthWebViewControllerWillDisappear() {
        
    }
    func oauthWebViewControllerDidDisappear() {
        // Ensure all listeners are removed if presented web view close
        oauthswift?.cancel()
    }
}
